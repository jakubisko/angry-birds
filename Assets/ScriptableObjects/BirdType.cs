using UnityEngine;

public class BirdType : ScriptableObject {

    public Sprite featherSprite;
    public bool canBeActivated;
    public string onSelectedPlaySoundName;
    public string onFiredPlaySoundName;
    public string onActivatedPlaySoundName;
    public string onCollisionPlaySoundName;

}

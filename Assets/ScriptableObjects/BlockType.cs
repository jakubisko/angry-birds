using UnityEngine;

public class BlockType : ScriptableObject {

    public float toughness;
    public string soundName;
    public float density;
    public Sprite blockFallApartSprite;
    public ParticleSystem blockDestructionEffect;

}

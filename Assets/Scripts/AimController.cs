using UnityEngine;

public class AimController : MonoBehaviour {

    public AimManager aimManager;

    private void Start() {
        GameEvents.singleton.onSlingDragging += (bird, initialVelocity) => {
            if (initialVelocity == Vector2.zero) {
                aimManager.ClearTrail();
            } else {
                aimManager.DrawTrailPrediction(bird, initialVelocity);
            }
        };
        GameEvents.singleton.onBirdFired += (bird, birdType) => aimManager.ClearTrail();
        GameEvents.singleton.onSlingDragCanceled += (bird, birdType) => aimManager.ClearTrail();
    }

}

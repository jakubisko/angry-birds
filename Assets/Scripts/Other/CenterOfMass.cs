using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CenterOfMass : MonoBehaviour {

    public Vector2 centerOfMass;

    private void Start() {
        GetComponent<Rigidbody2D>().centerOfMass = centerOfMass;
    }

}

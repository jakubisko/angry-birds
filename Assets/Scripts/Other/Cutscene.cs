using System.Collections;
using UnityEngine;

public class Cutscene : MonoBehaviour {

    private const float stoppedDuration = 2.8f; // Happens twice
    private const float scrollingDuration = 5;

    public Camera cam;
    public GameObject image;
    public TransitionManager transitionManager;

    private void Start() {
        StartCoroutine(ScrollCameraFromLeftToRight());
    }

    private IEnumerator ScrollCameraFromLeftToRight() {
        var spriteWidth = image.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
        var screenWidth = 2 * cam.orthographicSize * cam.aspect;
        var offset = (screenWidth - spriteWidth) / 2;
        cam.transform.position = new Vector2(offset, 0);

        yield return new WaitForSeconds(stoppedDuration);

        float elapsedTime = 0;
        while (elapsedTime < scrollingDuration) {
            cam.transform.position = new Vector2(offset * Mathf.Cos(Mathf.PI * elapsedTime / scrollingDuration), 0);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        yield return new WaitForSeconds(stoppedDuration);

        transitionManager.NextScene();
    }

}

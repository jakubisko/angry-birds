using UnityEngine;

public class Parallax : MonoBehaviour {

    public float parallaxSpeed;

    private Camera cam;
    private Vector3 start;

    private void Start() {
        cam = Camera.main;
        start = transform.position;
    }

    private void LateUpdate() {
        transform.position = start + cam.transform.position * parallaxSpeed;
    }

}

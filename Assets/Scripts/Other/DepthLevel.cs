/**
 * Holds constants defining z-coordinates of objects so that they are ordered as we want.
 */
public static class DepthLevel {

    /** The object most in front. */
    public const float camera = -10;
    public const float score = -6;
    public const float explosion = -5;
    public const float feather = -4;
    public const float blockFragment = -3;
    public const float egg = -2;
    // Sling has depth -1
    // Hats, helmets etc has depth -0.1
    // Pigs and objects have depth 0
    public const float trail = 2;

}

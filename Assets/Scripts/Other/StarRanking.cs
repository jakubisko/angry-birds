using UnityEngine;

/** Place this script in a Scene to define the score required to get full ranking in stars. */
public class StarRanking : MonoBehaviour {

    public int scoreForBestRank;

}

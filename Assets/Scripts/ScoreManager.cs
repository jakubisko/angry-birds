using System;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour {

    public static ScoreManager singleton { get; private set; }

    public const int scoreForDestroyedPig = 5_000;
    public const int scoreForDestroyedBlock = 500;
    public const int scoreForUnusedBird = 10_000;
    private const int maxStars = 3;

    public TMP_Text totalScoreText;
    public GameObject flyingScore;

    private int score = 0;

    private void Awake() {
        singleton = this;
    }

    public void AddScore(Vector2 position, int value) {
        score += value;
        totalScoreText.text = score.ToString();

        var position3 = new Vector3(position.x, position.y, DepthLevel.score);
        var obj = Instantiate(flyingScore, position3, Quaternion.identity);
        obj.GetComponent<FlyingScore>().SetValue(value);
    }

    /** Converts current score to stars. */
    public int GetCurrentStars() {
        var starRanking = FindObjectOfType<StarRanking>();
        var scoreRequired = (starRanking == null) ? 0 : starRanking.scoreForBestRank;

        for (int stars = maxStars; stars > 1; stars--) {
            if (score >= scoreRequired) {
                return stars;
            }
            scoreRequired -= scoreForUnusedBird;
        }
        return 1;
    }

    public string StarsAsText(int stars) {
        return (stars == 0) ? "" : (new String('★', stars) + new String('☆', ScoreManager.maxStars - stars));
    }

    /** Level number is user readable number starting from 1. */
    public void SaveStarsForLevel(int levelNumber, int noOfStars) {
        PlayerPrefs.SetInt("Level" + levelNumber, noOfStars);
        PlayerPrefs.Save(); // WebGL requires this
    }

    /** Level number is user readable number starting from 1. */
    public int GetSavedStarsForLevel(int levelNumber) {
        return PlayerPrefs.GetInt("Level" + levelNumber, 0);
    }

}

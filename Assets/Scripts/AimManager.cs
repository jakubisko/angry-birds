using UnityEngine;
using System.Collections.Generic;

/** Draws the planned path of the next bird. */
public class AimManager : MonoBehaviour {

    public GameObject trailObject;
    public float secondsBetweenDots;
    public int noOfDots;

    private List<GameObject> dots = new List<GameObject>();
    private float timeOffset;

    public void DrawTrailPrediction(GameObject bird, Vector2 initialVelocity) {
        for (int i = 0; i < noOfDots; i++) {
            float time = (i + timeOffset) * secondsBetweenDots;
            Vector2 position = (Vector2)bird.transform.position + time * (initialVelocity + time * Physics2D.gravity / 2);

            if (dots.Count < noOfDots) {
                GameObject dot = Instantiate(trailObject, position, Quaternion.identity);
                dots.Add(dot);
            } else {
                dots[i].transform.position = position;
            }
        }
    }

    public void ClearTrail() {
        dots.ForEach(obj => Destroy(obj));
        dots.Clear();
    }

    private void Update() {
        timeOffset = (timeOffset + Time.deltaTime) % 1;
    }

}

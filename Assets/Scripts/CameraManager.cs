using UnityEngine;
using System.Linq;

/**
 * Move the camera by the pan/drag gesture.
 * Zoom with the the pinch/spread gesture and the mouse wheel.
 * Camera may also follow some object.
 */
public class CameraManager : MonoBehaviour {

    /** You can lock the camera to disable its movement. Useful for example during aiming. */
    [HideInInspector]
    public bool cameraLocked;

    /** Camera will be following this object. To stop following it, set this to null or click the screen while it's not locked. */
    [HideInInspector]
    public GameObject objectToFollow;

    private const float cameraSpeed = 30;
    private const float wheelZoomingSpeed = 1;
    private const float minCamSize = 4;

    /** Where on screen should be the followed object. (-1, -1) is the top left corner. (+1, +1) is the bottom right corner of the screen. */
    private readonly Vector2 followedObjectScreenPosition = new Vector2(-0.5f, 0);

    private Camera cam;
    private Vector3 dragOrigin;

    private void Start() {
        cam = Camera.main;

        // Focus the camera in the middle of pigs.
        cam.transform.position = CalculateMiddleOfPigs();
        ClampCameraPosition();
    }

    private Vector2 CalculateMiddleOfPigs() {
        var positions = GameObject.FindGameObjectsWithTag("Pig").Select(pig => pig.transform.position);
        return positions.Aggregate(Vector3.zero, (sum, pos) => sum + pos) / positions.Count();
    }

    private void Update() {
        if (!cameraLocked &&
                Input.mousePosition.x >= 0 && Input.mousePosition.x < Screen.width &&
                Input.mousePosition.y >= 0 && Input.mousePosition.y < Screen.height) {

            ScrollByDragging();
            FollowObject();
            Zooming();
            ClampCameraPosition();
        }
    }

    private void ScrollByDragging() {
        if (Input.GetMouseButtonDown(0)) {
            dragOrigin = cam.ScreenToWorldPoint(Input.mousePosition);
            objectToFollow = null;
        }

        if (Input.GetMouseButton(0)) {
            var delta = dragOrigin - cam.ScreenToWorldPoint(Input.mousePosition);
            cam.transform.position += delta;
        }
    }

    private void FollowObject() {
        if (objectToFollow != null) {
            var targetPos = objectToFollow.transform.position
                + Vector3.left * followedObjectScreenPosition.x * cam.orthographicSize * cam.aspect
                + Vector3.up * followedObjectScreenPosition.y * cam.orthographicSize;
            cam.transform.position += Vector3.ClampMagnitude(targetPos - cam.transform.position, cameraSpeed * Time.deltaTime);
        }
    }

    private void Zooming() {
        float newSize = cam.orthographicSize;

        if (Input.touchCount == 2) {
            // Zoom by pinch/spread touch gesture.
            Touch touch1 = Input.touches[0];
            Touch touch2 = Input.touches[1];

            Vector2 touch1PrevPos = touch1.position - touch1.deltaPosition;
            Vector2 touch2PrevPos = touch2.position - touch2.deltaPosition;

            float prevSize = (touch1PrevPos - touch2PrevPos).magnitude;
            float currSize = (touch1.position - touch2.position).magnitude;

            newSize *= prevSize / currSize;
        } else {
            // Zoom by mouse wheel.
            float wheelDelta = Input.GetAxis("Mouse ScrollWheel");
            newSize *= (1 - wheelDelta * wheelZoomingSpeed);
        }

        cam.orthographicSize = Mathf.Clamp(
            newSize,
            minCamSize,
            (BoundaryManager.instance.maxY - BoundaryManager.instance.minY) / 2
        );
    }

    private void ClampCameraPosition() {
        cam.transform.position = new Vector3(
            middleClamp(
                cam.transform.position.x,
                BoundaryManager.instance.minX + cam.orthographicSize * cam.aspect,
                BoundaryManager.instance.maxX - cam.orthographicSize * cam.aspect
            ),
            middleClamp(
                cam.transform.position.y,
                BoundaryManager.instance.minY + cam.orthographicSize,
                BoundaryManager.instance.maxY - cam.orthographicSize
            ),
            DepthLevel.camera
        );
    }

    private float middleClamp(float value, float min, float max) {
        return min > max ? ((min + max) / 2) : Mathf.Clamp(value, min, max);
    }

}

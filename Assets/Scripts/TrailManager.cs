using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/** Draws the path the last bird took. */
public class TrailManager : MonoBehaviour {

    public GameObject trailObject;
    public float distanceBetweenDots;

    private GameObject birdToFollow;
    private List<GameObject> dots = new List<GameObject>();

    public void ClearTrail() {
        dots.ForEach(dot => Destroy(dot));
        dots.Clear();
    }

    public void StartTrail(GameObject bird) {
        birdToFollow = bird;
    }

    public void EndTrail() {
        birdToFollow = null;
    }

    public void MarkActivation() {
        if (birdToFollow == null) {
            return;
        }

        Vector3 curPos = birdToFollow.transform.position;
        GameObject dot = Instantiate(trailObject, curPos, Quaternion.identity);
        dot.transform.localScale = Vector3.one;
        dots.Add(dot);
    }

    private void Update() {
        if (birdToFollow == null) {
            return;
        }

        Vector3 curPos = birdToFollow.transform.position;
        if (dots.Count == 0 || (dots.Last().transform.position - curPos).sqrMagnitude >= distanceBetweenDots * distanceBetweenDots) {
            GameObject dot = Instantiate(trailObject, curPos, Quaternion.identity);
            dots.Add(dot);
        }
    }

}

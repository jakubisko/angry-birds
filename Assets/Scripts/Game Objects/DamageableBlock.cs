using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class DamageableBlock : MonoBehaviour, DamageResponsive {

    public BlockType blockType;
    [Tooltip("Ordered from the least damaged to the most damaged.")]
    public List<Sprite> damagedSprites;

    private SpriteRenderer spriteRenderer;

    private void Start() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        GetComponent<Rigidbody2D>().mass *= blockType.density;
    }

    public float getToughness() {
        return blockType.toughness;
    }

    public string getSoundName() {
        return blockType.soundName;
    }

    public void onDamage(float damageAmount, float healthLeft) {
        var value = Mathf.FloorToInt(ScoreManager.scoreForDestroyedBlock * damageAmount / 10) * 10;
        if (value > 0) {
            ScoreManager.singleton.AddScore(transform.position, value);
        }

        if (healthLeft > 0) {
            for (int i = damagedSprites.Count; i >= 1; i--) {
                if (healthLeft * (damagedSprites.Count + 1) <= i) {
                    spriteRenderer.sprite = damagedSprites[i - 1];
                    break;
                }
            }
        } else {
            BlockFallApart();
        }
    }

    private void BlockFallApart() {
        var position = new Vector3(transform.position.x, transform.position.y, DepthLevel.blockFragment);
        ParticleSystem particleSystem = Instantiate(blockType.blockDestructionEffect, position, gameObject.transform.rotation);

        ParticleSystem.ShapeModule shapeModule = particleSystem.shape;
        SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        shapeModule.sprite = spriteRenderer.sprite;

        ParticleSystem.EmissionModule emissionModule = particleSystem.emission;
        int count = Mathf.CeilToInt(16 * GetComponent<Rigidbody2D>().mass / blockType.density);
        ParticleSystem.Burst burst = new ParticleSystem.Burst(0, count);
        emissionModule.SetBursts(new ParticleSystem.Burst[] { burst });

        ParticleSystem.TextureSheetAnimationModule textureSheetAnimationModule = particleSystem.textureSheetAnimation;
        textureSheetAnimationModule.AddSprite(blockType.blockFallApartSprite);
    }

}

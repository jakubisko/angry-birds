using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Bird))]
public class BirdBlue : MonoBehaviour {

    private const float splitForce = 2;

    public void Activate() {
        var bird = GetComponent<Bird>();
        bird.MakePuff(12);

        var rb = GetComponent<Rigidbody2D>();
        for (int i = -1; i <= 1; i += 2) {
            var that = Instantiate(gameObject, transform.position, transform.rotation);
            var thatRb = that.GetComponent<Rigidbody2D>();
            thatRb.velocity = rb.velocity;
            thatRb.AddForce(splitForce * rb.mass * i * Vector2.up, ForceMode2D.Impulse);

            // Disable activation of the children. Otherwise, they would split to infinity.
            that.GetComponent<Bird>().onActivate = null;
        }
    }

}

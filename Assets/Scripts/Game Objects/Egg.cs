using UnityEngine;

public class Egg : MonoBehaviour {

    private const float fallSpeed = 20;
    private const float explosionRadius = 3.5f;
    private const float explosionForce = 35;

    private void Start() {
        GetComponent<Rigidbody2D>().velocity = Vector2.down * fallSpeed;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        ExplosionManager.singleton.MakeExplosion(transform.position, explosionRadius, explosionForce);
        Destroy(gameObject);
    }

}

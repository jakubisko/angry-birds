using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public enum SlingState {
    SlingLoading, // Animation of bird jumping to the Sling chair. Player can't shoot.
    SlingReady, // Sling is loaded with a bird. Player can shoot, but has not yet started dragging.
    SlingDragged, // Player is dragging a loaded sling, about to shoot a bird.
    Flying, // Bird is flying; player can't shoot. The camera should follow the bird; we're checking until everything stops moving.
    LevelEnded // Level ended. Player can't shoot.
}

[RequireComponent(typeof(Collider2D))]
public class Sling : MonoBehaviour {

    private const float firePower = 10;
    private const float cancelFireUnderDistance = 0.3f; // User can cancel firing the sling by dragging it less than this distance.
    private const float loadingAnimationDuration = 0.6f;

    public GameObject slingChair;
    public List<LineRenderer> slingLines;

    private Camera cam;
    private SlingState slingState;
    private CircleCollider2D dragCollider;
    private GameObject birdInChair;
    private float birdInChairRadius = 0;
    private List<GameObject> birdsInLine; // Birds not yet used by the player that may be fired in the future. Their collisions are disabled.
    private CursorController cursorController;

    private void Start() {
        cam = Camera.main;
        dragCollider = GetComponent<CircleCollider2D>();
        cursorController = FindObjectOfType<CursorController>(true);

        birdsInLine = GameObject.FindGameObjectsWithTag("Bird")
            .OrderBy(item => -Math.Abs(transform.position.x - item.transform.position.x))
            .ToList();
        foreach (GameObject bird in birdsInLine) {
            bird.GetComponent<Collider2D>().enabled = false;
            bird.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
        }

        GameEvents.singleton.onDefeat += () => slingState = SlingState.LevelEnded;
        GameEvents.singleton.onVictoryBeforeAnimation += () => {
            slingState = SlingState.LevelEnded;
            StartCoroutine(VictoryAnimation());
        };

        Invoke("StartLoadSlingWithNextBird", 1);
    }

    // Update is called after all OnMouse... events
    private void Update() {
        if (slingState == SlingState.SlingDragged && Input.GetMouseButtonUp(0)) {
            Fire();
        }

        UpdateSlingLines();

        if (slingState == SlingState.Flying && Array.TrueForAll(FindObjectsOfType<Rigidbody2D>(), rb => rb.IsSleeping())) {
            StartLoadSlingWithNextBird();
        }
    }

    private void StartLoadSlingWithNextBird() {
        StartCoroutine(SlingLoading());
    }

    private IEnumerator SlingLoading() {
        if (birdsInLine.Count == 0) {
            slingState = SlingState.LevelEnded;
            yield break;
        }

        slingState = SlingState.SlingLoading;

        GameObject bird = birdsInLine.Last();
        var startPos = bird.transform.position;
        float elapsedTime = 0;
        while (elapsedTime < loadingAnimationDuration) {
            bird.transform.position = Vector3.Lerp(startPos, transform.position, elapsedTime / loadingAnimationDuration)
                + Vector3.up * 4 * Mathf.Sin(Mathf.PI * elapsedTime / loadingAnimationDuration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        birdsInLine.RemoveAt(birdsInLine.Count - 1);
        birdInChair = bird;
        birdInChair.transform.position = transform.position;
        birdInChairRadius = birdInChair.GetComponent<CircleCollider2D>().radius;

        dragCollider.enabled = true;
        birdInChair.GetComponent<Collider2D>().enabled = false;
        slingState = SlingState.SlingReady;

        GameEvents.singleton.SlingReloaded(bird, bird.GetComponent<Bird>().birdType);
    }

    private void UpdateSlingLines() {
        Vector3 chairPosition = (birdInChair == null ? transform.position : birdInChair.transform.position)
          + Vector3.left * birdInChairRadius;

        foreach (LineRenderer line in slingLines) {
            line.SetPosition(0, chairPosition - transform.position);
        }

        slingChair.transform.position = chairPosition;
    }

    private void OnMouseDrag() {
        if (birdInChair == null) {
            return;
        }

        if (slingState != SlingState.SlingDragged) {
            slingState = SlingState.SlingDragged;
            var birdType = birdInChair.GetComponent<Bird>().birdType;
            GameEvents.singleton.SlingDragStarted(birdInChair, birdType);
            GameEvents.singleton.BirdSelected(birdInChair, birdType);
        }

        Vector3 mousePos = cam.ScreenToWorldPoint(Input.mousePosition);

        // Check whether the bird's path won't intersect the sling foot.
        // If it does, we need to clamp the distance in order to not hit the foot.
        ContactFilter2D contactFilter = new ContactFilter2D().NoFilter();
        RaycastHit2D[] hits = new RaycastHit2D[2];
        int hitCount = Physics2D.CircleCast(transform.position, birdInChairRadius + 0.05f, mousePos - transform.position, contactFilter, hits, dragCollider.radius);

        // hits[0] will always be the "dragCollider" at 0 distance. Skip it.
        float maxLength = hitCount > 1 ? hits[1].distance : dragCollider.radius;
        Vector3 delta = Vector3.ClampMagnitude((Vector2)(mousePos - transform.position), maxLength);
        birdInChair.transform.position = transform.position + delta;
        GameEvents.singleton.SlingDragging(birdInChair, delta.magnitude < cancelFireUnderDistance ? Vector3.zero : -firePower * delta);
    }

    private void Fire() {
        Vector3 delta = (Vector2)(transform.position - birdInChair.transform.position);
        if (delta.magnitude < cancelFireUnderDistance) {
            BirdType birdType = birdInChair.GetComponent<Bird>().birdType;
            GameEvents.singleton.SlingDragCanceled(birdInChair, birdType);

            slingState = SlingState.SlingReady;
        } else {
            dragCollider.enabled = false;
            birdInChair.GetComponent<Collider2D>().enabled = true;

            Rigidbody2D birdRigidbody2D = birdInChair.GetComponent<Rigidbody2D>();
            birdRigidbody2D.bodyType = RigidbodyType2D.Dynamic;

            birdRigidbody2D.velocity = firePower * delta;

            birdInChair.GetComponent<Animator>().SetTrigger("Fire");
            BirdType birdType = birdInChair.GetComponent<Bird>().birdType;
            GameEvents.singleton.BirdFired(birdInChair, birdType);
            birdInChair = null;

            slingState = SlingState.Flying;
        }
    }

    private IEnumerator VictoryAnimation() {
        yield return new WaitForSeconds(1.6f);
        foreach (GameObject bird in birdsInLine) {
            GameEvents.singleton.BirdSelected(bird, bird.GetComponent<Bird>().birdType);
            ScoreManager.singleton.AddScore(bird.transform.position + 2 * Vector3.up, ScoreManager.scoreForUnusedBird);
            yield return new WaitForSeconds(1.6f);
        }

        GameEvents.singleton.VictoryAnimationEnded();
    }

}

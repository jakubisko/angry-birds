using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Bird))]
public class BirdYellow : MonoBehaviour {

    private const float forceOnSpeedUp = 9;

    public void Activate() {
        var bird = GetComponent<Bird>();
        bird.MakePuff(12);

        var rb = GetComponent<Rigidbody2D>();
        rb.AddForce(rb.velocity.normalized * forceOnSpeedUp * rb.mass, ForceMode2D.Impulse);
    }

}

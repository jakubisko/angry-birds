using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(DamageResponsive))]
public class Damageable : MonoBehaviour {

    /** Give a short time to loosely placed blocks to settle down at the start of the scene. */
    private const float immortalTime = 0.5f;

    public const float damagePerAcceleration = 3f / 30f;
    private const float damagePerAngularAcceleration = 0.03f / 30f;
    private const float minCollisionDamage = 0.05f; // Lesser damage will be ignored. Higher is threated as a small "Collision".
    private const float minDamageDamage = 0.3f; // Higher damage is threatened as a serious "Damage".

    private Rigidbody2D rigidBody;
    private DamageResponsive damageResponsive;
    private Vector2 lastVelocity;
    private float lastAngularVelocity;
    private float health = 1;

    private void Start() {
        rigidBody = GetComponent<Rigidbody2D>();
        damageResponsive = GetComponent<DamageResponsive>();
    }

    private void FixedUpdate() {
        lastVelocity = rigidBody.velocity;
        lastAngularVelocity = rigidBody.angularVelocity;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (Time.timeSinceLevelLoad < immortalTime) {
            return;
        }

        float acceleration = (rigidBody.velocity - lastVelocity).magnitude / Time.fixedDeltaTime;
        float angularAcceleration = Mathf.Abs(rigidBody.angularVelocity - lastAngularVelocity) / Time.fixedDeltaTime;
        float damageAmount = (acceleration * damagePerAcceleration + angularAcceleration * damagePerAngularAcceleration);

        Damage(damageAmount);

        Damageable otherDamageable = other.gameObject.GetComponent<Damageable>();
        if (otherDamageable) {
            otherDamageable.Damage(damageAmount);
        }
    }

    public void Damage(float damageAmount) {
        damageAmount /= damageResponsive.getToughness();

        // This check prevents infinite cycle.
        // TNT creates explosion when it runs out of health.
        // That explosion would damage the TNT and create another explosion etc.
        if (damageAmount > minCollisionDamage && health > 0) {
            string objectName = damageResponsive.getSoundName();

            damageAmount = Mathf.Min(damageAmount, health);
            health -= damageAmount;
            damageResponsive.onDamage(damageAmount, health);

            if (health <= 0) {
                GameEvents.singleton.DamageDealt(objectName, "Destroyed");
                Destroy(gameObject);
            } else {
                string damageName = damageAmount < minDamageDamage ? "Collision" : "Damage";
                GameEvents.singleton.DamageDealt(objectName, damageName);
            }
        }
    }

}

using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Bird))]
[RequireComponent(typeof(Collider2D))]
public class BirdBlack : MonoBehaviour {

    private const float explosionRadius = 4.5f;
    private const float explosionForce = 45;

    public void Activate() {
        ExplosionManager.singleton.MakeExplosion(transform.position, explosionRadius, explosionForce);
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        GetComponent<Animator>().SetTrigger("Activate");
    }

}

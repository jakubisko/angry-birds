using UnityEngine;

public class DamageableTnt : MonoBehaviour, DamageResponsive {

    private const float explosionRadius = 4;
    private const float explosionForce = 40;

    public float getToughness() {
        return 10;
    }

    public string getSoundName() {
        return "Wood";
    }

    public void onDamage(float damageAmount, float healthLeft) {
        if (healthLeft <= 0) {
            ExplosionManager.singleton.MakeExplosion(transform.position, explosionRadius, explosionForce);
        }
    }

}

using UnityEngine;
using UnityEngine.Events;

/** Releases feathers on collisions. Destroys the bird and releases feather once it stops moving. */
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CircleCollider2D))]
public class Bird : MonoBehaviour {

    /** Sometimes a bird just keeps slowly rolling. This destroys him after some time. */
    private const float haltVelocity = 0.04f;
    private const float haltTimeThreshold = 1.1f; // This should be more than Black Bird's countdown timer.

    public ParticleSystem puffOfFeathersEffect;
    public BirdType birdType;
    public UnityEvent onActivate;

    private Rigidbody2D rb;
    private Animator animator;
    private float haltedTime;
    private float radius;
    private bool canActivate;
    private bool activateNow = false;

    // This has to be Awake and not just Start.
    // Blue bird is created mid-flight and immediately triggers a collision, requiring access to "rb".
    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        radius = GetComponent<CircleCollider2D>().radius;
        canActivate = birdType.canBeActivated;

        ResetBlinkTimer();
    }

    /** Called at the end of the animation. */
    public void ResetBlinkTimer() {
        animator.Update(Random.Range(0f, 3f));
    }

    private void OnCollisionEnter2D(Collision2D other) {
        canActivate = false;
        animator.SetTrigger("Damage");

        // Release a few feathers
        MakePuff(Random.Range(8, 14));

        GameEvents.singleton.BirdCollided(gameObject, birdType);
    }

    /** Bird activation. */
    private void Update() {
        if (!rb.isKinematic && canActivate && Input.GetMouseButtonDown(0)) {
            canActivate = false;
            activateNow = true;
            animator.SetTrigger("Activate");
        }
    }

    /** If the bird is idle, destroy it and release a lot of feathers. */
    private void FixedUpdate() {
        if (!rb.isKinematic) {
            if (activateNow) {
                activateNow = false;
                onActivate?.Invoke();
                GameEvents.singleton.BirdActivated(gameObject, birdType);
            }

            if (rb.IsSleeping() || rb.velocity.magnitude <= haltVelocity) {
                haltedTime += Time.fixedDeltaTime;
            } else {
                haltedTime = 0;
            }

            if (haltedTime > haltTimeThreshold) {
                MakePuff(30);
                Destroy(gameObject);
            }
        }
    }

    public ParticleSystem MakePuff(int feathersPerUnitSquare) {
        var position = new Vector3(transform.position.x, transform.position.y, DepthLevel.feather);
        ParticleSystem particleSystem = Instantiate(puffOfFeathersEffect, position, Quaternion.identity);

        ParticleSystem.EmissionModule emissionModule = particleSystem.emission;
        int burstSize = Mathf.RoundToInt(feathersPerUnitSquare * radius * radius);
        ParticleSystem.Burst burst = new ParticleSystem.Burst(0, burstSize);
        emissionModule.SetBursts(new ParticleSystem.Burst[] { burst });

        ParticleSystem.TextureSheetAnimationModule textureSheetAnimationModule = particleSystem.textureSheetAnimation;
        textureSheetAnimationModule.AddSprite(birdType.featherSprite);

        return particleSystem;
    }

    private void OnDestroy() {
        GameEvents.singleton.BirdDestroyed(gameObject, birdType);
    }

}

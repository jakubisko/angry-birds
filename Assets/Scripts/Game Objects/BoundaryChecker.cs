using UnityEngine;

/** If a GameObject with this component ventures too far from the middle of the scene, it will get destroyed. */
public class BoundaryChecker : MonoBehaviour {

    private void FixedUpdate() {
        // We don't check maxY - everything that goes up will eventually fall down anyway.
        if (transform.position.x < BoundaryManager.instance.minX ||
            transform.position.x > BoundaryManager.instance.maxX ||
            transform.position.y < BoundaryManager.instance.minY) {
            Destroy(gameObject);
        }
    }

}

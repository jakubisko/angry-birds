public interface DamageResponsive {

    float getToughness();
    string getSoundName();
    void onDamage(float damageAmount, float healthLeft);

}

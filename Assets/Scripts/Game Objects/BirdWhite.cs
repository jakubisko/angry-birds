using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Bird))]
public class BirdWhite : MonoBehaviour {

    public Transform egg;

    public void Activate() {
        // Change bird
        var rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(20, 20);
        rb.angularVelocity += 1800;

        GetComponent<CircleCollider2D>().radius = 0.6f;

        // Create egg
        var position = new Vector3(transform.position.x, transform.position.y, DepthLevel.egg);
        Instantiate(egg, position, Quaternion.identity);
    }

}

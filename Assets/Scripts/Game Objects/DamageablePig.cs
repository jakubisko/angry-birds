using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DamageablePig : MonoBehaviour, DamageResponsive {

    [Tooltip("Provide extra health for bigger pigs.")]
    public float toughness;

    private Animator animator;

    private void Start() {
        animator = GetComponent<Animator>();
        GameEvents.singleton.onDefeat += BecomeHappy;

        ResetBlinkTimer();
    }

    public void ResetBlinkTimer() {
        animator.Update(Random.Range(0f, 3f));
        GameEvents.singleton.PigOinked(gameObject);
    }

    public float getToughness() {
        return toughness;
    }

    public string getSoundName() {
        return "Pig";
    }

    public void onDamage(float damageAmount, float healthLeft) {
        this.animator.SetFloat("health", healthLeft);
        if (healthLeft <= 0) {
            ExplosionManager.singleton.MakeExplosion(transform.position, 0, 0);
            ScoreManager.singleton.AddScore(transform.position, ScoreManager.scoreForDestroyedPig);
        }
    }

    private void BecomeHappy() {
        this.animator.SetBool("happy", true);
    }

    private void OnDestroy() {
        GameEvents.singleton.onDefeat -= BecomeHappy;

        GameEvents.singleton.PigDestroyed(gameObject);
    }

}

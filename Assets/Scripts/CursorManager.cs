using UnityEngine;

public class CursorManager : MonoBehaviour {

    public Texture2D cursorDefault;
    public Texture2D cursorDrag;
    public Texture2D cursorPoint;

    private Texture2D currentCursor;
    private bool isOverButton;

    private void ChangeCursor() {
        Cursor.SetCursor(isOverButton ? cursorPoint : currentCursor, Vector2.zero, CursorMode.ForceSoftware);
    }

    public void UseCursorDefault() {
        currentCursor = cursorDefault;
        ChangeCursor();
    }

    public void UseCursorDrag() {
        currentCursor = cursorDrag;
        ChangeCursor();
    }

    public void UseCursorPoint() {
        currentCursor = cursorPoint;
        ChangeCursor();
    }

    public void UseCursorOverButton() {
        isOverButton = true;
        ChangeCursor();
    }

    public void UseCursorNotOverButton() {
        isOverButton = false;
        ChangeCursor();
    }

}

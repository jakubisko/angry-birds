using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionManager : MonoBehaviour {

    /** Scene 0 is intro screen. Scene 1 is start cutscene with pigs stealing eggs. */
    public const int NoOfUnplayableScenesBefore = 2;
    /** Scene N-2 is the final cutscene with pigs beaten. Scene N-1 is the image seen when you close the application in WebGL. */
    public const int NoOfUnplayableScenesAfter = 2;
    /** Scene N-1 is the image seen when you close the application in WebGL. It can't be accessed by normal gameplay. */
    public const int NoOfInaccessibleScenesAfter = 1;

    /** This will go to through both levels and cutscenes. It wil go to intro screen after the end of the game. */
    public void NextScene() {
        int sceneNumber = (SceneManager.GetActiveScene().buildIndex + 1) % (SceneManager.sceneCountInBuildSettings - NoOfInaccessibleScenesAfter);
        SceneManager.LoadSceneAsync(sceneNumber);
    }

    public void RetryLevel() {
        int sceneBuildIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadSceneAsync(sceneBuildIndex);
    }

    /** Level number is user readable number starting from 1. */
    public void GoToLevel(int levelNumber) {
        int sceneNumber = (levelNumber + NoOfUnplayableScenesBefore - 1) % (SceneManager.sceneCountInBuildSettings - NoOfInaccessibleScenesAfter);
        SceneManager.LoadSceneAsync(sceneNumber);
    }

    /** When you quit the game in WebGL, it freezes on the last image. We have one screen designed for that. */
    public void GoToQuitScreen() {
        SceneManager.LoadScene(SceneManager.sceneCountInBuildSettings - NoOfInaccessibleScenesAfter);
    }

    public int GetNumberOfPlayableLevels() {
        return SceneManager.sceneCountInBuildSettings - NoOfUnplayableScenesBefore - NoOfUnplayableScenesAfter;
    }

    /** Returns user readable number of the current level; starting from 1. */
    public int GetCurrentLevelNumber() {
        return SceneManager.GetActiveScene().buildIndex - NoOfUnplayableScenesBefore + 1;
    }

}

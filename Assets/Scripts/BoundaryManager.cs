using System.Linq;
using UnityEngine;

/** The borders of the scene are determined at the start of the scene from the positions of RigidBody2Ds on the scene. */
public class BoundaryManager : MonoBehaviour {

    public static BoundaryManager instance { get; private set; }

    private const float extraBoundary = 8;

    public float minX { get; private set; }
    public float maxX { get; private set; }
    public float minY { get; private set; }
    public float maxY { get; private set; }

    private void Awake() {
        instance = this;

        var items = FindObjectsOfType<Rigidbody2D>();
        minX = items.Select(rb => rb.position.x).Min() - extraBoundary;
        maxX = items.Select(rb => rb.position.x).Max() + extraBoundary;
        minY = -5; // TODO - hardcoded
        maxY = items.Select(rb => rb.position.y).Max() + extraBoundary;
    }

}

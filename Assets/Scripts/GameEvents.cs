using System;
using UnityEngine;

public class GameEvents : MonoBehaviour {

    public static GameEvents singleton { get; private set; }

    private void Awake() {
        singleton = this;
    }

    public delegate void BirdEvent(GameObject bird, BirdType birdType);

    public event BirdEvent onSlingReloaded;
    public void SlingReloaded(GameObject bird, BirdType birdType) {
        onSlingReloaded?.Invoke(bird, birdType);
    }

    public event BirdEvent onSlingDragStarted;
    public void SlingDragStarted(GameObject bird, BirdType birdType) {
        onSlingDragStarted?.Invoke(bird, birdType);
    }

    /** Sling has a velocity threshold. If the user doesn't drag it far enough, the initial velocity will be zero and firing will be canceled. */
    public delegate void SlingDraggingEvent(GameObject bird, Vector2 initialVelocity);
    public event SlingDraggingEvent onSlingDragging;
    public void SlingDragging(GameObject bird, Vector2 initialVelocity) {
        onSlingDragging?.Invoke(bird, initialVelocity);
    }

    /** Sling has a velocity threshold. If the user doesn't drag it far enough, the initial velocity will be zero and firing will be canceled. */
    public event BirdEvent onSlingDragCanceled;
    public void SlingDragCanceled(GameObject bird, BirdType birdType) {
        onSlingDragCanceled?.Invoke(bird, birdType);
    }

    public event BirdEvent onBirdSelected;
    public void BirdSelected(GameObject bird, BirdType birdType) {
        onBirdSelected?.Invoke(bird, birdType);
    }

    public event BirdEvent onBirdFired;
    public void BirdFired(GameObject bird, BirdType birdType) {
        onBirdFired?.Invoke(bird, birdType);
    }

    public event BirdEvent onBirdActivated;
    public void BirdActivated(GameObject bird, BirdType birdType) {
        onBirdActivated?.Invoke(bird, birdType);
    }

    public event BirdEvent onBirdCollided;
    public void BirdCollided(GameObject bird, BirdType birdType) {
        onBirdCollided?.Invoke(bird, birdType);
    }

    public event Action<string, string> onDamageDealt;
    public void DamageDealt(string objectName, string damageName) {
        onDamageDealt?.Invoke(objectName, damageName);
    }

    public event Action<float> onExplosion;
    public void Explosion(float force) {
        onExplosion?.Invoke(force);
    }

    public event BirdEvent onBirdDestroyed;
    public void BirdDestroyed(GameObject bird, BirdType birdType) {
        onBirdDestroyed?.Invoke(bird, birdType);
    }

    public event Action<GameObject> onPigOinked;
    public void PigOinked(GameObject pig) {
        onPigOinked?.Invoke(pig);
    }

    public event Action<GameObject> onPigDestroyed;
    public void PigDestroyed(GameObject pig) {
        onPigDestroyed?.Invoke(pig);
    }

    public event Action onVictoryBeforeAnimation;
    public void VictoryBeforeAnimation() {
        onVictoryBeforeAnimation?.Invoke();
    }

    public event Action onVictoryAnimationEnded;
    public void VictoryAnimationEnded() {
        onVictoryAnimationEnded?.Invoke();
    }

    public event Action onDefeat;
    public void Defeat() {
        onDefeat?.Invoke();
    }

    /** Once application ends, objects will begin to tear down. We no longer want any event handlers to trigger. */
    private void OnApplicationQuit() {
        onSlingReloaded = null;
        onSlingDragStarted = null;
        onBirdSelected = null;
        onBirdFired = null;
        onBirdActivated = null;
        onBirdCollided = null;
        onDamageDealt = null;
        onExplosion = null;
        onBirdDestroyed = null;
        onPigOinked = null;
        onPigDestroyed = null;
        onVictoryBeforeAnimation = null;
        onVictoryAnimationEnded = null;
        onDefeat = null;
    }

}

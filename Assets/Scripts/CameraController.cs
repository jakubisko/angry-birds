using UnityEngine;
using System.Linq;

public class CameraController : MonoBehaviour {

    public CameraManager cameraManager;

    private void Start() {
        GameEvents.singleton.onSlingReloaded += (bird, birdType) => cameraManager.objectToFollow = bird;
        GameEvents.singleton.onSlingDragStarted += (bird, birdType) => cameraManager.cameraLocked = true;
        GameEvents.singleton.onBirdFired += (bird, birdType) => {
            cameraManager.cameraLocked = false;
            cameraManager.objectToFollow = bird;
        };
        GameEvents.singleton.onBirdActivated += (bird, birdType) => {
            if (bird.GetComponent<BirdWhite>()) {
                var egg = GameObject.FindWithTag("Egg");
                cameraManager.objectToFollow = egg;
            }
        };
        GameEvents.singleton.onVictoryBeforeAnimation += () => {
            var birdInLine = GameObject.FindGameObjectsWithTag("Bird")
                .FirstOrDefault(bird => bird.GetComponent<Rigidbody2D>().isKinematic);
            if (birdInLine != null) {
                cameraManager.objectToFollow = birdInLine;
            }
        };
    }

}

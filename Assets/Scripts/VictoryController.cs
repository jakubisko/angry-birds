using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class VictoryController : MonoBehaviour {

    private bool isLevelFinished;
    private float timeToCheck;

    private void Start() {
        GameEvents.singleton.onBirdDestroyed += (bird, birdType) => CheckVictoryDefeatInNearFuture();
        GameEvents.singleton.onPigDestroyed += pig => CheckVictoryDefeatInNearFuture();

        SceneManager.sceneLoaded += (a, b) => isLevelFinished = false;
    }

    private void CheckVictoryDefeatInNearFuture() {
        // Since Destroy is not called immediately, execute the check only after some delay.
        // Implementation with Invoke threw errors at the end of the level even when cancelled. So don't use it.
        timeToCheck = 0.1f;
    }

    private void Update() {
        if (timeToCheck > 0) {
            timeToCheck -= Time.deltaTime;
            if (timeToCheck <= 0) {
                CheckVictoryDefeat();
            }
        }
    }

    private void CheckVictoryDefeat() {
        if (isLevelFinished) {
            return;
        }

        if (GameObject.FindWithTag("Pig") == null) {
            // If there are no more pigs, win immediately. No need to wait for blocks to stop moving.
            isLevelFinished = true;
            GameEvents.singleton.VictoryBeforeAnimation();
        } else if (GameObject.FindWithTag("Bird") == null) {
            // If there are still pigs and no more birds, wait until blocks stop moving. Some block may still fall and kill the last pig.
            if (Array.TrueForAll(FindObjectsOfType<Rigidbody2D>(), rb => rb.IsSleeping())) {
                isLevelFinished = true;
                GameEvents.singleton.Defeat();
            } else {
                timeToCheck = 0.1f;
            }
        }
    }

}

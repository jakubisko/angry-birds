using UnityEngine;
using UnityEngine.SceneManagement;

public class CursorController : MonoBehaviour {

    public CursorManager cursorManager;

    private void Start() {
        GameEvents.singleton.onSlingDragStarted += (bird, birdType) => cursorManager.UseCursorDrag();
        GameEvents.singleton.onBirdFired += (bird, birdType) => {
            if (birdType.canBeActivated) {
                cursorManager.UseCursorPoint();
            } else {
                cursorManager.UseCursorDefault();
            }
        };
        GameEvents.singleton.onSlingDragCanceled += (bird, birdType) => cursorManager.UseCursorDefault();
        GameEvents.singleton.onBirdActivated += (bird, birdType) => cursorManager.UseCursorDefault();
        GameEvents.singleton.onBirdCollided += (bird, birdType) => cursorManager.UseCursorDefault();
        GameEvents.singleton.onBirdDestroyed += (bird, birdType) => cursorManager.UseCursorDefault();

        cursorManager.UseCursorDefault();
        SceneManager.sceneLoaded += (a, b) => cursorManager.UseCursorNotOverButton();
    }

}

using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

    public GameObject victoryMenu;
    public GameObject defeatMenu;
    public GameObject level1Button;
    public Text currentLevelsStars;
    public TransitionManager transitionManager;
    public ScoreManager scoreManager;

    private void Start() {
        GameEvents.singleton.onDefeat += () => defeatMenu.SetActive(true);
        GameEvents.singleton.onVictoryAnimationEnded += () => {
            DisplayAndSaveStarsForCurrentLevel();
            victoryMenu.SetActive(true);
        };
    }

    public void CreateLevelButtons() {
        if (level1Button.transform.parent.childCount > 1) {
            return;
        }

        // Level 1 button already exists. Duplicate it to create rest of the buttons.
        level1Button.GetComponentInChildren<Text>().text = scoreManager.StarsAsText(scoreManager.GetSavedStarsForLevel(1));
        for (int i = 2; i <= transitionManager.GetNumberOfPlayableLevels(); i++) {
            var obj = Instantiate(level1Button, level1Button.transform.parent);
            obj.GetComponentInChildren<TMP_Text>().text = i.ToString();
            obj.GetComponentInChildren<Text>().text = scoreManager.StarsAsText(scoreManager.GetSavedStarsForLevel(i));
        }
    }

    private void DisplayAndSaveStarsForCurrentLevel() {
        int stars = scoreManager.GetCurrentStars();
        currentLevelsStars.text = scoreManager.StarsAsText(stars);

        int levelNumber = transitionManager.GetCurrentLevelNumber();
        if (stars > scoreManager.GetSavedStarsForLevel(levelNumber)) {
            scoreManager.SaveStarsForLevel(levelNumber, stars);
        }
    }

    public void GoToLevelBasedOnButtonCaption(TMP_Text text) {
        var levelNumber = Int32.Parse(text.text);
        transitionManager.GoToLevel(levelNumber);
    }

    public void ExitGame() {
        transitionManager.GoToQuitScreen();

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}

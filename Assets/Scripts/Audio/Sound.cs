using UnityEngine;

[System.Serializable]
public class Sound {

    public string name;
    [Range(0, 1)]
    public float volume;

    /** One of these clips will be played at random. */
    public AudioClip[] clips;

    [HideInInspector]
    public AudioSource[] sources;

}

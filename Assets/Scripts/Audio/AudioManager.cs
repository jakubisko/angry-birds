using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class AudioManager : MonoBehaviour {

    [Range(0, 1)]
    public float masterVolume;
    public Sound[] sounds;

    private void Awake() {
        foreach (Sound sound in sounds) {
            sound.sources = new AudioSource[sound.clips.Length];
            for (int i = 0; i < sound.clips.Length; i++) {
                AudioSource source = gameObject.AddComponent<AudioSource>();
                source.clip = sound.clips[i];
                source.volume = sound.volume * masterVolume;
                sound.sources[i] = source;
            }
        }
    }

    /** This allows null or empty argument. Nothing will be played in that case. */
    public void Play(string name) {
        if (String.IsNullOrEmpty(name)) {
            return;
        }

        Sound sound = Array.Find(sounds, s => s.name == name);
        if (sound == null) {
            Debug.LogWarning("Sound with name '" + name + "' not found!");
        } else {
            int index = Random.Range(0, sound.sources.Length);
            var source = sound.sources[index];
            if (source != null) { // Source may be null during Scene transitions.
                source.Play();
            }
        }
    }

}

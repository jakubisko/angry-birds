using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioController : MonoBehaviour {

    public AudioManager audioManager;

    private void Start() {
        GameEvents.singleton.onSlingDragStarted += (bird, birdType) => audioManager.Play("SlingDragged");
        GameEvents.singleton.onBirdSelected += (bird, birdType) => audioManager.Play(birdType.onSelectedPlaySoundName);
        GameEvents.singleton.onBirdFired += (bird, birdType) => audioManager.Play("SlingFire");
        GameEvents.singleton.onBirdFired += (bird, birdType) => audioManager.Play(birdType.onFiredPlaySoundName);
        GameEvents.singleton.onBirdActivated += (bird, birdType) => audioManager.Play(birdType.onActivatedPlaySoundName);
        GameEvents.singleton.onBirdCollided += (bird, birdType) => audioManager.Play(birdType.onCollisionPlaySoundName);
        GameEvents.singleton.onDamageDealt += (objectName, damageName) => audioManager.Play(objectName + damageName);
        GameEvents.singleton.onExplosion += force => { if (force > 0) audioManager.Play("Explosion"); };
        GameEvents.singleton.onBirdDestroyed += (bird, birdType) => audioManager.Play("BirdDestroyed");
        GameEvents.singleton.onPigOinked += pig => audioManager.Play("PigOink");
        GameEvents.singleton.onVictoryBeforeAnimation += () => audioManager.Play("Victory");
        GameEvents.singleton.onVictoryAnimationEnded += () => audioManager.Play("VictoryJingle");
        GameEvents.singleton.onDefeat += () => audioManager.Play("Defeat");

        int sceneBuildIndex = SceneManager.GetActiveScene().buildIndex;
        audioManager.Play(sceneBuildIndex == 0 ? "Theme" : "Start");
    }

}

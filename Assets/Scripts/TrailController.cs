using UnityEngine;

public class TrailController : MonoBehaviour {

    public TrailManager trailManager;

    private void Start() {
        GameEvents.singleton.onBirdFired += (bird, birdType) => { trailManager.ClearTrail(); trailManager.StartTrail(bird); };
        GameEvents.singleton.onBirdActivated += (bird, birdType) => trailManager.MarkActivation();
        GameEvents.singleton.onBirdCollided += (bird, birdType) => trailManager.EndTrail();
        GameEvents.singleton.onBirdDestroyed += (bird, birdType) => trailManager.EndTrail();
    }

}

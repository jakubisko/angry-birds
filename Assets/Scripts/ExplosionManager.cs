using UnityEngine;

public class ExplosionManager : MonoBehaviour {

    public static ExplosionManager singleton { get; private set; }

    public Transform explosion;

    private void Awake() {
        singleton = this;
    }

    public void MakeExplosion(Vector2 position, float radius, float force) {
        var position3 = new Vector3(position.x, position.y, DepthLevel.explosion);
        Instantiate(explosion, position3, Quaternion.identity);

        if (radius > 0 && force > 0) {
            var colliders = Physics2D.OverlapCircleAll(position, radius);
            foreach (Collider2D collider in colliders) {
                Rigidbody2D rb = collider.GetComponent<Rigidbody2D>();
                Damageable damageable = collider.GetComponent<Damageable>();
                if (damageable && rb) {
                    AddExplosionForce(rb, force, position, radius, 2, ForceMode2D.Impulse);
                    DealExplosionDamage(damageable, force, position, radius);
                }
            }
        }

        GameEvents.singleton.Explosion(force);
    }

    private void AddExplosionForce(Rigidbody2D rb, float explosionForce, Vector2 explosionPosition, float explosionRadius, float upwardsModifier = 0.0F, ForceMode2D mode = ForceMode2D.Force) {
        var explosionDir = rb.position - explosionPosition;
        var explosionDistance = explosionDir.magnitude;

        // Normalize without computing magnitude again
        if (upwardsModifier == 0)
            explosionDir /= explosionDistance;
        else {
            // From Rigidbody.AddExplosionForce doc:
            // If you pass a non-zero value for the upwardsModifier parameter, the direction
            // will be modified by subtracting that value from the Y component of the centre point.
            explosionDir.y += upwardsModifier;
            explosionDir.Normalize();
        }

        float coef = Mathf.Lerp(0, explosionForce, 1 - explosionDistance / explosionRadius);
        rb.AddForce(coef * explosionDir, mode);
    }

    private void DealExplosionDamage(Damageable damageable, float explosionForce, Vector2 explosionPosition, float explosionRadius) {
        var explosionDir = ((Vector2)damageable.transform.position) - explosionPosition;
        var explosionDistance = explosionDir.magnitude;

        float coef = Mathf.Lerp(0, explosionForce, 1 - explosionDistance / explosionRadius);
        damageable.Damage(coef * explosionForce * Damageable.damagePerAcceleration);
    }

}

using UnityEngine;
using System.Collections;
using TMPro;

public class FlyingScore : MonoBehaviour {

    public TMP_Text scoreText;

    private float maxScale = 1;
    private float animationDuration = 0.5f;

    private void Awake() {
        StartCoroutine(FlyingScoreResizing());
    }

    public void SetValue(int value) {
        maxScale += value / ScoreManager.scoreForDestroyedPig;
        animationDuration += value / ScoreManager.scoreForDestroyedPig / 2;
        scoreText.text = value.ToString();
    }

    private IEnumerator FlyingScoreResizing() {
        float elapsedTime = 0;
        while (elapsedTime < animationDuration) {
            transform.localScale = Vector3.one * maxScale
                * Mathf.Min(1, 1.5f * Mathf.Sin(Mathf.PI * elapsedTime / animationDuration));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        Destroy(gameObject);
    }

}
